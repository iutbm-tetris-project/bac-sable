package fr.univ_fcomte.iut_bm.sandbox;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        StackPane sp = new StackPane();
        Scene scene = new Scene(sp, 640, 480);

        Image tetrisChan = new Image("/tetris-chan.png");
        ImageView tetrisChanView = new ImageView(tetrisChan);
        sp.getChildren().add(tetrisChanView);

        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(5.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.BLACK);

        Label label = new Label("Coucou tout le monde !");
        label.setTextFill(Color.WHITE);
        label.setFont(Font.font(32));
        label.setEffect(dropShadow);
        sp.getChildren().add(label);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        // Force font anti-aliasing/smoothing
        System.setProperty("prism.lcdtext", "false");
        launch();
    }
}
